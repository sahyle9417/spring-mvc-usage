package springmvc.springmvcusage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcUsageApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcUsageApplication.class, args);
	}

}
