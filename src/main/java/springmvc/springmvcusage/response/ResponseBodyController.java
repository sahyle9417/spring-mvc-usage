package springmvc.springmvcusage.response;

import springmvc.springmvcusage.basic.MyData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// Class 에 Controller 대신 RestController 사용하면 메서드에 ResponseBody 생략 가능
@Slf4j
@RestController
public class ResponseBodyController {

    @GetMapping("/response-body-string-v1")
    public void responseBodyV1(HttpServletResponse response) throws IOException {
        response.getWriter().write("ok");
    }

    @GetMapping("/response-body-string-v2")
    public ResponseEntity<String> responseBodyV2() {
        return new ResponseEntity<>("ok", HttpStatus.OK);
    }

//    @ResponseBody
    @GetMapping("/response-body-string-v3")
    public String responseBodyV3() {
        return "ok";
    }

    // 권장
    @GetMapping("/response-body-json-v1")
    public ResponseEntity<MyData> responseBodyJsonV1() {
        MyData myData = new MyData();
        myData.setUsername("userA");
        myData.setAge(20);
        return new ResponseEntity<>(myData, HttpStatus.OK);
    }
    
    // 권장하지 않음 (소스에서 응답코드 설정 못함)
    // @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/response-body-json-v2")
    public MyData responseBodyJsonV2() {
        MyData myData = new MyData();
        myData.setUsername("userA");
        myData.setAge(20);
        return myData;
    }

}
