package springmvc.springmvcusage.basic;

import lombok.Data;

// @Getter @Setter @RequiredArgsConstructor @ToString @EqualsAndHashCode
@Data
public class MyData {
    private String username;
    private int age;
}
